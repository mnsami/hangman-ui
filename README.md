Hangman-ui
======

This is a frontend, for the  [hangman-api](https://gitlab.com/mnsami/hangman-api) backend.


### 1. Frameworks and tools used
1. `AngularJs: 1.5.11`
2. Bootstrap.


### Installations
1. checkout the project
`git clone git@gitlab.com:mnsami/hangman-ui.git`
2. Just copy and paste `hangman-ui` to any web server root directory.
3. Replace `PATH_TO_HANGMAN_API` in `app.js` with the url for the `hangman-api`.

### Screenshots
1. Homepage (unregistered)  
![Image of homepage_unregistered](./screenshots/hangman_unregistered.jpg)

2. Registration  
![Image of registration](./screenshots/hangman_register.jpg)

3. Homepage (registered)  
![Image of homepage](./screenshots/hangman_homepage.jpg)

4. Choose Game  
![Image of choose game](./screenshots/hangman_choose.jpg)

5. Hangman game play  
![Image of game play](./screenshots/hangman_game_play.jpg)

6. Player won game  
![Image of player won](./screenshots/hangman_won.jpg)

