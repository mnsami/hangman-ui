'use strict';

// Declare app level module which depends on views, and components
var hangmanApp = angular.module('hangmanUiApp', [
  'ngRoute',
  'ngComponentRouter'
]);

hangmanApp.constant('hangman_api_url', 'PATH_TO_HANGMAN_API');
hangmanApp.constant('hangman_api_player_header', 'X-Hangman-Player-uuid');

hangmanApp.config(['$locationProvider', '$httpProvider', function ($locationProvider, $httpProvider) {
  $locationProvider.html5Mode(true);

  $httpProvider.defaults.headers.post = {};
  $httpProvider.defaults.headers.get = {};
}]);

hangmanApp.value('$routerRootComponent', 'hangmanUiApp');

hangmanApp.run(['playersService', function(playersService) {
  // Get the current user when the application starts
  // (in case they are still logged in from a previous session)
  playersService.requestCurrentUser();
}]);

hangmanApp.component('hangmanUiApp', {
  $routeConfig: [
    {path: '/', name: 'Homepage', component: 'homepage', useAsDefault: true},
    {path: '/register', name: 'Register', component: 'registration'},
    {path: '/games', name: 'Games', component: 'games'},
    {path: '/hangman', name: 'Hangman', component: 'hangmanGame'}
  ],
  template: '<navbar></navbar><div class="container"><ng-outlet></ng-outlet></div>'
});
