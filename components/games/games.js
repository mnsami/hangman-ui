'use strict';

hangmanApp.component('games', {
  templateUrl: 'components/games/games.html',
  bindings: { $router: '<' },
  controller: function(gamesService) {
    var $ctrl = this;

    this.getHangmanBannerImage = function() {
      return "assets/images/hangman_banner.jpg";
    }

    this.startHangman = function() {
      this.$router.navigate(['Hangman']);
    }

    this.$routerOnActivate = function (next) {
      console.log('games.$routerOnActivate');
    }
  }
});
