'use strict';

hangmanApp.component('hangmanGame', {
    templateUrl: 'components/games/hangman/hangman.html',
    bindings: { $router: '<' },
    controller: function(playersService, gamesService) {
        var $ctrl = this;
        $ctrl.letters = ["0","1","2","3","4","5","6","7","8","9",'a','b',"c","d","e","f","g","h","i",
            "j","k","l","m","n","o","p","q","r","s","t","u",
            "v","w","x","y","z"];
        $ctrl.exceptionError = null;
        $ctrl.chars = [];
        $ctrl.failedAttempts = 0;
        $ctrl.gameDisable = false;
        var gameStatus = null;
        var wrongLetters = [];
        var correctLetters = [];

        this.isGameWin = function () {
            return (gameStatus === "WIN");
        }

        this.isGameLoose = function () {
            return (gameStatus === "LOOSE");
        }

        this.getStickFigureImage = function () {
            return "assets/images/hangman_step_" + $ctrl.failedAttempts + ".jpg";
        }

        this.refreshValues = function(game) {
            correctLetters = game.letters;
            wrongLetters = game.wrongLetters;
            $ctrl.failedAttempts = game.failedAttempts;
            $ctrl.chars = game.word.split("");
            gameStatus = game.status;
            if (game.status === "WIN" || game.status === "LOOSE") {
                playersService.refreshCurrentUser();
                $ctrl.gameDisable = true;
            }
        };

        this.$routerOnActivate = function (next) {
            gamesService.startNewGame().then(function (response) {
                $ctrl.game = response;
                $ctrl.refreshValues(response);
            }, function (error) {
                $ctrl.exceptionError = error.data.errorMessage;
            });
        };

        this.showGame = function() {
            var show = false;
            if (!$ctrl.exceptionError && $ctrl.game) {
                show = true;
            }
            return show;
        };


        this.playTurn = function (char) {
            gamesService.playTurn(char, $ctrl.game.id).then(function (response){
                $ctrl.game = response;
                $ctrl.refreshValues(response);
            }, function (error){
                $ctrl.exceptionError = error.data.errorMessage;
            });
        };

        this.getRemainingFailedAttempts = function () {
            return (5 - $ctrl.failedAttempts);
        }

        this.isLetterWrong = function (letter) {
            var wrongIdx = wrongLetters.indexOf(letter);

            return (-1 !== wrongIdx);
        }

        this.isLetterChosen =  function (letter) {
            // check if pressed letter is in the wrong letters
            var wrongIdx = wrongLetters.indexOf(letter);

            // check if pressed letter is in the correct word letters
            var isCorrectLetterFound = correctLetters.some(function (value, index) {
                var keys = Object.keys(value);
                var letterIdx = keys.indexOf(letter);
                if (-1 !== letterIdx) {
                    $ctrl.revealChar(letter, Object.values(value));
                    return true;
                }

                return false;
            });

            var result = (isCorrectLetterFound || -1 !== wrongIdx);
            return result;
        };

        this.revealChar = function (char, indexes) {
            indexes.forEach(function (value){
                value.forEach(function (key) {
                    var idx = key;
                    $ctrl.chars[idx] = char;
                });
            });
        }

        this.goToHomepage = function () {
            this.$router.navigate(['Homepage']);
        };
    }
});
