'use strict';

hangmanApp.component('homepage', {
    templateUrl: 'components/homepage/homepage.html',
    bindings: { $router: '<' },
    controller: function(playersService) {
        var $ctrl = this;
        $ctrl.topGames = null;
        $ctrl.topPlayers = null;

        this.getRegisterLink = function() {
            return 'Register';
        }

        this.$routerOnActivate = function(next) {
            if (playersService.isAuthenticated()) {
                $ctrl.player = playersService.requestCurrentUser();
                playersService.getTopGames().then(function(response) {
                    $ctrl.topGames = response;
                });

                playersService.getTopPlayers().then(function(response) {
                    $ctrl.topPlayers = response;
                });
            }
        };

        this.isAuthenticated = function() {
            return playersService.isAuthenticated();
        };
    }
});
