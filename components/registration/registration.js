'use strict';

hangmanApp.component('registration', {
  templateUrl: 'components/registration/registration.html',
  bindings: { $router: '<' },
  controller: function (playersService) {
    // The model for this form
    var $ctrl = this;

    this.$routerOnActivate = function (next) {
      console.log('registration.$routerOnActivate');
    }

    this.clearForm = function() {
      $ctrl.player = {};
    };

    this.goToProfile = function () {
      this.$router.navigate(['Homepage']);
    };

    this.register = function() {

      // Try to register
      playersService.register($ctrl.player.username, $ctrl.player.age).then(function(response) {
        if (response) {
          $ctrl.goToProfile();
        }
      }, function(x) {
        console.log(x);
        // If we get here then there was a problem with the login request to the server
        $ctrl.exceptionError = "Failed due to exception: " + x.data.errorMessage;
      });
    };
  }
});
